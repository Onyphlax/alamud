from .event import Event2

class HelpEvent(Event2):
    NAME = "help"

    def perform(self):
        if not self.object.has_prop("helpable"):
            self.fail()
            return self.inform("help.failed")
        self.inform("help")
