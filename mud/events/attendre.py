from .event import Event1

class WaitEvent(Event1):
    NAME = "wait"

    def perform(self):
        self.inform("wait")
        cont = self.actor.container()
        for x in list(self.actor.contents()):
            x.move_to(cont)
        self.actor.move_to(None)